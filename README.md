# Node File Upload with Sequelize

This is a simple project that I made using multer, sequelize, jwt to upload files throught rest to a backend


> To upload a file you need to use a multipart post request to the site/post address

> all queries except /auth/register and /auth/authenticate needs to have an auth header 
>{authorization: "bearer token"} where token is a token that you can get from the auth routes

some queries

/auth/register

```json
{
	"email": "email",
	"username": "username",
	"password": "password"
}

```

/auth/authenticate
```json
{
	"email": "email",
	"password": "password"
}
```

# Changelog

##1.0.1
 * Created the route /posts/:key
 * added user_info to the json when you retrieve the post from the routes
 * fixed some error like deleting without authorization

