const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

generateToken = (params = {}) => {
    return jwt.sign(
        params,
        process.env.JWT_SECRET,
        {
            expiresIn: (1000*60*process.env.JWT_EXPIRATION_IN_MINUTES) //1 s * 60 * jwt_expiration in minutes
        }
    )
}

module.exports = {
    async register(req, res) {
        const {email} = req.body;
        try {
            if(await User.findOne({where: {email}})){
                return res.status(400).json({error: 'User already exist'});
            }   
            const user = await User.create(req.body);
            user.password = undefined;
            return res.send({user, token: generateToken({id : user.id})});
        } catch(err) {
            return res.sendStatus(400);
        }
    },

    async authenticate(req,res) {
        const { email, password} = req.body;

        const user = await User.findOne({where: {email}});
        

        if(!user)
            return res.status(400).send({error:"User does not exists"});

        if(!await bcrypt.compare(password, user.password)){
            return res.status(400).send({error: 'invalid password'})
        }

        user.password = undefined;

        res.send({user, token: generateToken({id : user.id})});
    }
};