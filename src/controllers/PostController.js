const Post = require('../models/Post');
const User = require('../models/User');

module.exports = {
    async index(req, res) {
        let posts;
        if (!req.params.key) {
             posts = await Post.findAll({ include: { association: 'user' } });
        }else{

            posts = await Post.findOne(
                {
                    where: { key: req.params.key },
                    attributes :{exclude: ['user_id']},
                    include: {  association: 'user',
                                attributes:['username'] 
                            }
                }
            );
        }

        if(!posts)
            return res.status(400).send({error: "Post does not exist"});
            
        return res.json(posts);
    },

    async store(req, res) {
        const { filename: name, size, key } = req.file

        userId = req.userId;


        const user = await User.findByPk(userId);

        if (!user)
            return res.status(400).send({ error: "User not found" });


        const post = await Post.create({
            name,
            size,
            key,
            url: ''
        });

        user.addPost(post);

        return res.json(post);
    },
    async remove(req, res) {
        const post = await Post.findOne({
            where: {
                key: req.params.key
            }
        });

        if(!post)
            return res.status(400).send({error: "File does not exists"});

        await post.destroy();

        return res.sendStatus(200);
    }
};