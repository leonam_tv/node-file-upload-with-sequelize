const path = require('path');
const fs = require('fs');
const { promisify } = require('util');

const { Model, DataTypes } = require('sequelize');

class Post extends Model {
    static init(sequelize) {
        super.init({
            key: DataTypes.STRING,
            name: DataTypes.STRING,
            size: DataTypes.INTEGER,
            createdAt: DataTypes.DATE,
            url: DataTypes.STRING
        }, {
            sequelize,
            tableName: "post"
        });

        this.addHook('beforeCreate', (post, options) => {
            post.url = (post.url === "") ? `${process.env.APP_URL}files/${post.name}` : post.url;
            post.createdAt =(post.createdAt === undefined || post.createdAt === null)? Date.now() : post.createdAt;
            
        });

        this.addHook('beforeDestroy', (post, options) => {
            if (process.env.STORAGE_TYPE === 'local') {
                return promisify(fs.unlink)(path.resolve(__dirname,'..','..','tmp','uploads',post.name));
            }
        });
    }

    static associate(models){
        this.belongsTo(models.User,{foreignKey:'user_id', as: 'user' });
        // this.belongsTo(models.User);
    }
    
}

module.exports = Post;