const { Model, DataTypes } = require('sequelize');
const bcrypt = require('bcryptjs');

class User extends Model {
    static init(sequelize) {
        super.init({
            email: {
                type: DataTypes.STRING,
                allowNull: false
            },
            username: {
                type: DataTypes.STRING,
                allowNull: false
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false
            }
        }, {
            sequelize,
            tableName: 'user'
        })

        this.addHook('beforeCreate', async (user, options) => {
            user.password = await bcrypt.hash(user.password, 10);
        })
    }

    
    static associate(models){
        this.hasMany(models.Post, {foreignKey: 'user_id', as: 'posts'});
    }
}

module.exports = User;