const routes = require('express').Router();
const multer = require('multer');
const multerConfig = require('./config/multer');

const authMiddleware = require('./middlewares/Auth');

const PostController = require('./controllers/PostController');
const authController = require('./controllers/AuthController');

//post routes
routes.get('/posts', PostController.index);
routes.get('/posts/:key',PostController.index);
routes.delete('/posts/:key',authMiddleware, PostController.remove);
routes.post('/posts',authMiddleware ,multer(multerConfig).single('file'),PostController.store);

//Auth routes
routes.post('/auth/register', authController.register);
routes.post('/auth/authenticate', authController.authenticate);

module.exports = routes;