const multer    = require('multer');
const path      = require('path');
const Post      = require('../models/Post');

const nameTest = (num) => {
    const dict = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    let identifier = '';
    let number = (num === null) ? 0 : num;
    let len = dict.length;

    let quocient = 0;
    let remainder = 0;

    while (number >= len) {
        quocient = parseInt(number / len);
        remainder = parseInt(number % len);
        identifier = dict[remainder] + identifier;
        number = quocient - 1;
    }
    identifier = dict[number] + identifier;
    return identifier;
}

const storageTypes = {
    local: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, path.resolve(__dirname, '..', '..', 'tmp', 'uploads'));
        },
        filename: async function (req, file, cb)
        {
            const key = nameTest(await Post.max('id'));
            file.key = ((key == 'undefined')? "a" : key);
            cb(null, (file.key + "-" + file.originalname));
        }

    }),
    other: {}
};

module.exports = {
    dest: path.resolve(__dirname, '..', '..', 'tmp', 'uploads'),
    storage: storageTypes[process.env.STORAGE_TYPE],
    limits: {
        fileSize: 2 * 1024 * 1024,
    },
    fileFilter: (req, file, cb) => {
        const allowMimes = ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'];
        if (allowMimes.includes(file.mimetype)) {
            cb(null, true);
        } else {
            cb(new Error('Invalid File type'));
        }

    }

}