const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization;
    
    if(!authHeader)
        res.status(401).send({error: 'No token provided'});
    
    const parts = authHeader.split(' ');
    
    if(!parts.lenght === 2)
        res.status(401).send({error: 'Token error'});

    const [scheme, token] = parts;

    if(!/^Bearer$/i.test(scheme)){
        res.status(401).send({error: 'Token malformatted'});
    }

    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
        if(err) {
            return res.status(401).send({error: 'invalid token'});
        }

        req.userId = decoded.id;

        return next();
    })
};