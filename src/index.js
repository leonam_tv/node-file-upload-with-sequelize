const express   = require('express');
const morgan    = require('morgan');
const path      = require('path');
require('dotenv').config();

//app routes
const routes    = require('./routes');
require('./database/index');


const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(morgan('dev'));

//serve static files(upload folder)
app.use('/files', express.static(path.resolve(__dirname,'..','tmp','uploads')));
app.use(routes);

app.listen(3000);